package origin;

import ru.andrik.easyunit2.Easy;
import ru.andrik.easyunit2.log.EasyLog;
import ru.andrik.easyunit2.obj.wrapper.Unwrap;
import ru.andrik.easyunit2.obj.wrapper.Wrap;
import ru.andrik.easyunit2.sql.EasySqlQuery;
import ru.andrik.easyunit2.str.EasyStr;
import ru.andrik.easyunit2.str.EnumEncoding;
import ru.wapland.games.Vars;
import ru.wapland.games._generic.entity.*;
import ru.wapland.games._generic.entity.GenericIEntityField.StructEntityField;
import ru.wapland.games._generic.sql.GenericSqlEntity;
import ru.wapland.games._generic.sql.ISqlEntityAsVersion;
import ru.wapland.games._generic.sql.ISqlEntityVersions;
import ru.wapland.games.controller.user.UserController;

import java.util.HashMap;

/** это просто для примера, в этом класе я постарался вынести наружу все что мог, для поникамия работы,
 * в моих реальных классах часть логики убрана в GenericEntity и верхний класс выглядит проще */
@AClassITS(moreTypes = "Test", isNotCompile = true)
public class TestEntity extends GenericEntity implements ISqlEntityVersions, ITestEntityAutoGetterSetter {

    protected int virtualGetInitType() {
        return 1;
    } // это просто для примера, вообще это будет abstract и будут перегружать наследники

    protected int virtualGetInitVersion() {
        return 1;
    } // это просто для примера, вообще это будет abstract и будут перегружать наследники

    /** вот это устаревшая версия класса, она умеет загружать устаревшую запись из базы данных */
    @AClassFieldsAutoGetterSetter
    protected abstract class TestEntitySqlLoadVersion extends ISqlEntityAsVersion.AbsSqlEntityAsVersion implements ITestEntityTestEntitySqlLoadVersionAutoGetterSetter {
        // тут я для примера перепутал все поля, чтобы показать как старая версия будет загружать данные для новой версии (это я пока делаю руками т.к. тут может быть своя логика при загрузке)
        @AFieldSqlPackedLong(i = 1, ii = {32, 63})
        void writeSqlLong1(final int p_set) {
            TestEntity.this.m_packedLong1 = p_set;
        }

        @AFieldSqlPackedLong(i = 1, ii = {0, 31})
        void writeSqlLong2(final int p_set) {
            TestEntity.this.m_packedLong2 = p_set;
        }

        @AFieldSqlPackedLong(i = 2, ii = 32)
        void writeSqlBool1(final boolean p_set) {
            TestEntity.this.m_packedBool1 = p_set;
        }

        @AFieldSqlPackedLong(i = 2, ii = 48)
        void writeSqlBool2(final boolean p_set) {
            TestEntity.this.m_packedBool2 = p_set;
        }

        @AFieldSqlPackedLong(i = 7, ii = {32, 63})
        void writeSqlShort1(final int p_set) {
            TestEntity.this.m_packedShort1 = (short) p_set;
        }

        @AFieldSqlPackedLong(i = 7, ii = {16, 23})
        void writeSqlByte1(final int p_set) {
            TestEntity.this.m_packedByte1 = (byte) p_set;
        }

        @AFieldSqlPackedStr(n = "OldStr1")
        void writeSqlOldStr1(final String p_set) {
            TestEntity.this.m_packedStr1 = p_set;
        }

        @AFieldSqlPackedStr(n = "OldStr2")
        void writeSqlOldStr2(final String p_set) {
            TestEntity.this.writeSqlPackedStr2(p_set);
        }

        @AFieldSqlPackedStr(n = "Items")
        void writeSqlItems(final byte[] p_set) {
            TestEntity.this.writeSqlItems(p_set);
        }
    }

    // вот эти поля и нужно атоматизировать (смотри авто интерфейс: ITestEntityAutoGetterSetter)

    // это поле запишет в базу данных предок GenericEntity, а вот клиенту я отправляю руками т.к. часто бывает логика при отправке
    @AITS
    private String tsId() {
        return getCodedId();
    } // это отправляется клиенту (нельзя отправлять клиенту реальные ID - всегда их нужно кодировать)

    @Unwrap
    void setr_id(final long p_id) {
        SqlId = p_id;
    } // это при сохранении в XML

    @Wrap
    long getr_id() {
        return SqlId;
    } // это при загрузке из XML

    // простое поле которое напрямую транслирется в базу данных и в старый Виртомир и клиенту (без каких либо преобразований) 
    @AField // это чтобы создались getter/setter
    @AFieldSql(sql = TestEntitySql.FSimpleLong) // это поле в таблице базы данных
    @AFieldWapLand(field = "Long") // это поле в старом виртомире
    @AITS // это значит нужно отправить клиенту
            long m_simpleLong; // и вот само поле для работы в Java (getter/setter создадутся автоматически)

    // простое поле которое преобазовывается (все методы находят нужные поля по имени после префиктов: read/write, readSql/writeSql, readWapLand/writeWapLand, в данном случае найдет UserId) 
    @AField
    long m_userId; // getter/setter

    @AFieldSql(sql = TestEntitySql.FUserId)
    int readSqlUserId() {
        return (int) (m_userId & 0xFFFFFFFFL);
    } // это просто для примера, понятно что можно было и напрямую присваивать int в long без этого метода т.к. у меня автоматическое преобразование типов друг в друга при работе с полями AField

    @AFieldSql
        // тут можно не указывать (sql=TestEntitySql.FUserId) т.к. это уже указано для read
    void writeSqlUserId(final int p_set) {
        m_userId = ((long) p_set) & 0xFFFFFFFFL;
    } // это просто для примера, понятно что можно было и напрямую присваивать int в long без этого метода т.к. у меня автоматическое преобразование типов друг в друга при работе с полями AField 

    @AFieldWapLand(field = "UserId")
        // будет искать в старом виртомире такое поле
    short readWapLandUserId() {
        return (short) (m_userId & 0xFFFFL);
    } // это просто для примера якобы в старом виртомире SqlId был short

    @AFieldWapLand
        // то же самое второй раз можно не указывать (field="UserId")
    void writeWapLandUserId(final short p_set) {
        m_userId = ((long) p_set) & 0xFFFF;
    } // это просто для примера

    @AITS
        // клиенту нельзя показывать реальный ID - поэтому всегда его нужно кодировать, чтобы клиент не мог подобрать запрос для получения скрытой информации абонента 
    String tsUserId() {
        return UserController.Impl.getEntityCodedId(m_userId);
    } // в реальности я еще и кэширую закодированные значения

    // запись в базу данных закодированного UserId (просто для примера)
    // тут указываем какое поле прослушивать на сохраннеие, и если его сохраняет - то сохранить и это поле тоже (самостоятельно это поле не сохраняется)
    @AFieldSql(sql = TestEntitySql.FUserCodedId, listenSqlSaveFields = "UserId")
    String readSqlCodedUserId() {
        return UserController.Impl.getEntityCodedId(m_userId);
    }

    // читать это поле из базы данных нет смысла
    @AFieldSql
    void writeSqlCodedUserId(final String p_set) {
    } // этот метод можно вообще не указывать

    // а вот так можно заставить создать static поле доступа к этому полу напрямую из интерфеса
    @AField(addGetField = true)
    int m_directInt; // теперь можно обратиться к этому полу так: ITestEntityAutoGetterSetter.stFieldDirectInt.getValue(this);
    // а вот так можно делать массив полей (такое иногда тоже нужно)
    @AField(ownArray = "stArray:=4")
    int[] m_array; // теперь к этим полям можно обратиться так: ITestEntityAutoGetterSetter.stArray[3].getValue(this);

    // бинарно упакованные поля (считается что это массив long[] и значения i=0 указывает на long[0], а значнеия ii - указывает на начало бита и конец бита включительно, можно выходить за границу long на следующий long)
    @AField(notSetter = true) // ему нельзя генерировать setter
    @AFieldSqlPackedLong(i = 0, ii = {0, 23}) // (24 бит)
    @AITS
    final public int Type = virtualGetInitType();

    @AFieldSqlPackedLong
    void writeSqlType(final int p_set) {
        if (p_set != Type) EasyLog.showError("При чтении из базы данных данных класса: " + this.getClass().getName() + ", ожидается Type=" + Type + ", а загружается=" + p_set);
    }

    @AField(notGetterSetter = true) // этому вообще нет смысла от getter/setter
    @AFieldSqlPackedLong(i = 0, ii = {24, 31}) // (1 байт) версия записи
    final public int Version = virtualGetInitVersion();

    @AFieldSqlPackedLong
    void writeSqlVersion(final int p_set) {
    } // ничего не делать

    @AField
    @AFieldSqlPackedLong(i = 1, ii = {0, 31})
    int m_packedLong1;

    @AField
    void writePackedLong1(final long p_set) {
        m_packedLong1 = (int) p_set;
    } // это просто чтобы показать как для таких полей создаются авто setter

    @AField
    @AFieldSqlPackedLong(i = 1, ii = {32, 63})
    int m_packedLong2;
    @AField
    @AFieldSqlPackedLong(i = 2, ii = 0)
    boolean m_packedBool1;
    @AField
    @AFieldSqlPackedLong(i = 2, ii = 1)
    boolean m_packedBool2;
    @AField
    @AFieldSqlPackedLong(i = 2, ii = {2, 33})
    short m_packedShort1;
    @AField
    @AFieldSqlPackedLong(i = 2, ii = {34, 41})
    byte m_packedByte1;

    // строки преобразующиеся в байты
    @AField
    @AFieldSqlPackedStr(n = "Str1")
    String m_packedStr1;
    @AField
    String m_packedStr2;

    @AFieldSqlPackedStr(n = "Str2")
    String readSqlPackedStr2() {
        return m_packedStr2 + "_test2";
    } // это просто для примера

    @AFieldSqlPackedStr
    void writeSqlPackedStr2(final String p_set) {
        m_packedStr2 = p_set.substring(0, p_set.length() - 6);
    } // это просто для примера

    @AField
    String m_packedStr3;

    @AFieldSqlPackedStr(n = "Str3")
    byte[] readSqlPackedStr3() {
        return EasyStr.toBytes(m_packedStr3, EnumEncoding.UTF16);
    } // это просто для примера

    @AFieldSqlPackedStr
    void writeSqlPackedStr3(final byte[] p_set) {
        m_packedStr3 = EasyStr.toStr(p_set, EnumEncoding.UTF16);
    } // это просто для примера

    // когда нужно содержать набор вложенных классов - тогда так:
    @AField(notGetterSetter = true) // запрещает автоматически создавать getter/setter
    @AFieldSqlPackedStr(n = "Items")
    byte[] readSqlItems() {
        return GenericSqlEntity.savePackedToBytes(m_items.values().iterator());
    } // это просто для примера

    @AFieldSqlPackedStr
    void writeSqlItems(final byte[] p_set) {
        GenericSqlEntity.loadPackedFromBytes(p_set, (p_bytes, p_offset, p_size) -> {
            return getController().createEntityFromBytes(p_bytes, p_offset, p_size);
        });
    } // это просто для примера

    transient HashMap<Integer, TestEntity> m_items = new HashMap<>(); // это просто для примера

    public TestEntity() {
    }

    @Override
    public void dispose() {
        super.dispose();
    }

    public boolean initialize() {
        this.getUserId(); // авто getter

        // вот пример как получается доступ к полям этого класса, this.getEntityInfoFields() - переопределяется в автоинтерфейсе, и есть возможность наследовать IEntityGetterSetter.StructStaticEntityFields и добавлять новые возможности 
        for(var l_field : this.getEntityInfoFields().ListFields) {
            EasyLog.showStr("поле: " + l_field.FieldName);
            EasyLog.showStr("значение этого поля: " + l_field.ValueRead.getValue(this));
            EasyLog.showStr("есть Sql: " + (l_field.Sql != null));
            EasyLog.showStr("есть SqlPackedLong: " + ((l_field.Sql != null) && (l_field.Sql.SqlPackedLong != null)));
            EasyLog.showStr("есть SqlPackedStr: " + ((l_field.Sql != null) && (l_field.Sql.SqlPackedStr != null)));
            EasyLog.showStr("есть WapLand: " + (l_field.WapLand != null));
            EasyLog.showStr("отправляется на клиент: " + (l_field.ITS != null));
        }
        this.setUserId(22); // это напрямую через авто setter
        this.getEntityInfoFields().MapFields.get("UserId").setValue(this, 25, true); // вот так можно програмно выставлять значения (например при загрузке из базы данных, или от клиента)
        // true - означает вызывать сохраннеие в базу данных и запоминать это изменение в очереди изменений

        return true;
    }

    /** этот метод нужен когда объект не упакован в байты, а напрямую транслируется в поля табицы базы данных */
    @Override
    public IEntityGetterSetter createSqlEntityLoadAsVersion(final EasySqlQuery p_query) {
        try {
            var l_ret = getController().createEntityVersion(this, p_query.fields.getInt(TestEntitySql.FVersion)); // это когда Version записана напрямую в поле SQL
            return (l_ret == null) ? this : (IEntityGetterSetter) l_ret;
        } catch (Exception e) {
            EasyLog.showError(e);
            return null;
        }
    }

    /** этот метод нужен когда объект упакован в байты, и загружается из набора байт */
    @Override
    public IEntityGetterSetter createSqlEntityLoadAsVersion(final byte[] p_bytes, final int p_offset) {
        try {
            IEntityGetterSetter l_ret = getController().createEntityVersion(this, getVersion(p_bytes, p_offset)); // это когда версия записана в упакованных битах
            return (l_ret != null) ? l_ret : this;
        } catch (Exception e) {
            EasyLog.showError(e);
            return null;
        }
    }

    @Override
    protected int virtualGetMaxSizeModifyedEntityFields() {
        return Vars.Impl.CountCacheModifyedEntityFields;
    }

    @Override
    public QueueModifyedEntityFields getQueueModifyedEntityITSFields(final boolean p_autoCreateIfNotExists) {
        if ((m_opt & ConstOptIsDisbaleSaveModifyedEntityITSFields) != 0) return null; // запрещено сохранять
        QueueModifyedEntityFields l_ret = m_modifyedEntityITSFields; // это очередь содержащая изменения данного объекта
        if (l_ret != null) return l_ret;
        synchronized (this) {
            if ((l_ret = m_modifyedEntityITSFields) == null) l_ret = m_modifyedEntityITSFields = new QueueModifyedEntityFields(virtualGetMaxSizeModifyedEntityFields());
        }
        return l_ret;
    }

    @Override
    public long getModifyIndex() {
        return ModifyIndex;
    }

    @Override
    public long updateModifyIndex() {
        return (ModifyIndex++);
    }

    @Override
    public long updateEntityModifyIndex(final StructEntityField p_field, final IEntityGetterSetter p_fieldValueImpl, final boolean p_isSqlSave) {
        long l_ret = updateModifyIndex();
        if ((!p_isSqlSave) || (p_field == null) || (p_field.ITS == null) || (!p_field.ITS.IsExistsRead)) return l_ret;
        var q = getQueueModifyedEntityITSFields(true);
        if (q != null) q.add(l_ret, p_field, p_fieldValueImpl);
        return l_ret;
    }

    @Override
    public TestEntityController getController() {
        return (TestEntityController) super.getController();
    }

    // это методы получающие из байт нужные значения (т.к. только эта сущъность знает что и куда она записала) наверное это нужно будет тоже автоматизировать, но мне пока лень
    static int getTypeInt(final byte[] p_bytes, final int p_offset) {
        return Easy.bytesToInt(p_bytes, p_offset + 4);
    } // всего 3 байта (24бита) - смотри: Type (+4 т.к. это берется из лонг младший int)

    static int getVersion(final byte[] p_bytes, final int p_offset) {
        return Easy.toUnsigned(p_bytes[p_offset + 4]);
    } // всего 1 байт - смотри: public final Version (+4 т.к. это берется из лонг младший int)

}
