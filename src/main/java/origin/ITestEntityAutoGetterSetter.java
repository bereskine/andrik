package origin;

import ru.wapland.games._generic.entity.GenericIEntityField.StructEntityField;
import ru.wapland.games._generic.entity.IEntityGetterSetter;

/** 2021-22-11 13:32 autocreated from class: ru.wapland.games.controller.test.TestEntity */
@SuppressWarnings("all")
public interface ITestEntityAutoGetterSetter extends IEntityGetterSetter, ru.wapland.games._generic.entity.IGenericEntityAutoGetterSetter {

    static final String[] stEN = new String[]{"PackedStr1", "DirectInt", "Type", "PackedStr3", "PackedStr2", "PackedBool1", "PackedLong2", "UserId", "Array", "Array0", "Array1", "Array2", "Array3", "SimpleLong", "Version", "PackedByte1", "PackedLong1", "PackedBool2", "PackedShort1", "SqlItems"};
    static final StructEntityField[] stEO = new StructEntityField[20];

    default public String getPackedStr1() {
        return t().m_packedStr1;
    }

    default public void setPackedStr1(final String p, final boolean p_saveSql) {
        TestEntity t = t();
        if (IEntityGetterSetter.isEquals(t.m_packedStr1, p)) return;
        t.m_packedStr1 = p;
        IEntityGetterSetter.onChange(t, stEO[0], p_saveSql);
    }

    default public void setPackedStr1(final String p) {
        setPackedStr1(p, true);
    }

    default public int getDirectInt() {
        return t().m_directInt;
    }

    default public void setDirectInt(final int p, final boolean p_saveSql) {
        TestEntity t = t();
        if (t.m_directInt == p) return;
        t.m_directInt = p;
        IEntityGetterSetter.onChange(t, stEO[1], p_saveSql);
    }

    default public void setDirectInt(final int p) {
        setDirectInt(p, true);
    }

    default public StructEntityField getDirectIntField() {
        return (StructEntityField) stEO[1];
    }

    default public int getType() {
        return t().Type;
    }

    default public String getPackedStr3() {
        return t().m_packedStr3;
    }

    default public void setPackedStr3(final String p, final boolean p_saveSql) {
        TestEntity t = t();
        if (IEntityGetterSetter.isEquals(t.m_packedStr3, p)) return;
        t.m_packedStr3 = p;
        IEntityGetterSetter.onChange(t, stEO[3], p_saveSql);
    }

    default public void setPackedStr3(final String p) {
        setPackedStr3(p, true);
    }

    default public String getPackedStr2() {
        return t().m_packedStr2;
    }

    default public void setPackedStr2(final String p, final boolean p_saveSql) {
        TestEntity t = t();
        if (IEntityGetterSetter.isEquals(t.m_packedStr2, p)) return;
        t.m_packedStr2 = p;
        IEntityGetterSetter.onChange(t, stEO[4], p_saveSql);
    }

    default public void setPackedStr2(final String p) {
        setPackedStr2(p, true);
    }

    default public boolean isPackedBool1() {
        return t().m_packedBool1;
    }

    default public void setPackedBool1(final boolean p, final boolean p_saveSql) {
        TestEntity t = t();
        if (t.m_packedBool1 == p) return;
        t.m_packedBool1 = p;
        IEntityGetterSetter.onChange(t, stEO[5], p_saveSql);
    }

    default public void setPackedBool1(final boolean p) {
        setPackedBool1(p, true);
    }

    default public int getPackedLong2() {
        return t().m_packedLong2;
    }

    default public void setPackedLong2(final int p, final boolean p_saveSql) {
        TestEntity t = t();
        if (t.m_packedLong2 == p) return;
        t.m_packedLong2 = p;
        IEntityGetterSetter.onChange(t, stEO[6], p_saveSql);
    }

    default public void setPackedLong2(final int p) {
        setPackedLong2(p, true);
    }

    default public long getUserId() {
        return t().m_userId;
    }

    default public void setUserId(final long p, final boolean p_saveSql) {
        TestEntity t = t();
        if (t.m_userId == p) return;
        t.m_userId = p;
        IEntityGetterSetter.onChange(t, stEO[7], p_saveSql);
    }

    default public void setUserId(final long p) {
        setUserId(p, true);
    }

    default public int[] getArray() {
        return t().m_array;
    }

    default public void setArray(final int[] p, final boolean p_saveSql) {
        TestEntity t = t();
        if (IEntityGetterSetter.isEquals(t.m_array, p)) return;
        t.m_array = p;
        IEntityGetterSetter.onChange(t, stEO[8], p_saveSql);
    }

    default public void setArray(final int[] p) {
        setArray(p, true);
    }

    default public long getSimpleLong() {
        return t().m_simpleLong;
    }

    default public void setSimpleLong(final long p, final boolean p_saveSql) {
        TestEntity t = t();
        if (t.m_simpleLong == p) return;
        t.m_simpleLong = p;
        IEntityGetterSetter.onChange(t, stEO[13], p_saveSql);
    }

    default public void setSimpleLong(final long p) {
        setSimpleLong(p, true);
    }

    default public byte getPackedByte1() {
        return t().m_packedByte1;
    }

    default public void setPackedByte1(final byte p, final boolean p_saveSql) {
        TestEntity t = t();
        if (t.m_packedByte1 == p) return;
        t.m_packedByte1 = p;
        IEntityGetterSetter.onChange(t, stEO[15], p_saveSql);
    }

    default public void setPackedByte1(final byte p) {
        setPackedByte1(p, true);
    }

    default public int getPackedLong1() {
        return t().m_packedLong1;
    }

    default public void setPackedLong1(final int p, final boolean p_saveSql) {
        TestEntity t = t();
        if (t.m_packedLong1 == p) return;
        t.writePackedLong1(p);
        IEntityGetterSetter.onChange(t, stEO[16], p_saveSql);
    }

    default public void setPackedLong1(final int p) {
        setPackedLong1(p, true);
    }

    default public boolean isPackedBool2() {
        return t().m_packedBool2;
    }

    default public void setPackedBool2(final boolean p, final boolean p_saveSql) {
        TestEntity t = t();
        if (t.m_packedBool2 == p) return;
        t.m_packedBool2 = p;
        IEntityGetterSetter.onChange(t, stEO[17], p_saveSql);
    }

    default public void setPackedBool2(final boolean p) {
        setPackedBool2(p, true);
    }

    default public short getPackedShort1() {
        return t().m_packedShort1;
    }

    default public void setPackedShort1(final short p, final boolean p_saveSql) {
        TestEntity t = t();
        if (t.m_packedShort1 == p) return;
        t.m_packedShort1 = p;
        IEntityGetterSetter.onChange(t, stEO[18], p_saveSql);
    }

    default public void setPackedShort1(final short p) {
        setPackedShort1(p, true);
    }

    private TestEntity t() {
        return ((TestEntity) this);
    }

    static final IEntityGetterSetter.StructStaticEntityFields stEntityInfoFields = IEntityGetterSetter.create(TestEntity.class, ITestEntityAutoGetterSetter.class);

    @Override
    default IEntityGetterSetter.StructStaticEntityFields getEntityInfoFields() {
        return stEntityInfoFields;
    }

    static public StructEntityField stFieldDirectInt = (StructEntityField) stEO[1];
    static final StructEntityField[] stArray = new StructEntityField[]{stEO[9], stEO[10], stEO[11], stEO[12]};
    static final boolean stIsCreated = IEntityGetterSetter.onCreated(stEntityInfoFields);

}
