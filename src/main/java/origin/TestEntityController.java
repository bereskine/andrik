package origin;

import ru.andrik.easyunit2.log.EasyLog;
import ru.wapland.games.VirtoGames;
import ru.wapland.games._common.Cache;
import ru.wapland.games._common.Coder;
import ru.wapland.games._generic.AController;
import ru.wapland.games._generic.StructDetectClassGetterSetterFromKey;
import ru.wapland.games._generic.StructDetectClassGetterSetterFromKey.StructCreaterGetterSetterFromKey;
import ru.wapland.games._generic.entity.GenericControllerEntity;
import ru.wapland.games._generic.entity.IEntityGetterSetter;
import ru.wapland.games.controller.ControllerEventers;

/** это просто для примера */
@AController(isAutoCreate = false) // руками загружается из настроек
public class TestEntityController<T extends TestEntity> extends GenericControllerEntity<T> {

    static public TestEntityController<TestEntity> Impl;

    protected StructCreaterGetterSetterFromKey m_createrTest;

    @Override
    public void dispose() {
        super.dispose();
    }

    @SuppressWarnings("unchecked")
    @Override
    public boolean initializeBefore() {
        Impl = (TestEntityController<TestEntity>) this;
        if (!super.initializeBefore()) return false;

        {
            m_createrTest = ControllerEventers.addDetectorClass(new StructDetectClassGetterSetterFromKey((p) -> {
                p.setFindName("Test", null);
                p.setAccessibleClass(TestEntity.class, (p_class) -> ((TestEntity) p.create(p_class)).Type);
                p.setAccessibleVersionClass(TestEntity.TestEntitySqlLoadVersion.class, (p_impl) -> ((TestEntity) p_impl).Type);
            }));
        }

        return true;
    }

    @Override
    public boolean initialize() {
        if (!super.initialize()) return false;

        return true;
    }

    @Override
    public boolean start() {
        if (!super.start()) return false;

        return true;
    }

    @Override
    public Coder getCoderId() {
        return VirtoGames.Impl.Coders.CityId;
    }

    @Override
    public T putToCache(final T p_entity) {
        if (p_entity == null) return null;
        if (p_entity.getControllerArrayIndex() == -1) p_entity.setControllerArrayIndex(this);
        T l_ret = Cache.Impl.putEntity(true, p_entity); // кладем его в память
        return l_ret;
    }

    @SuppressWarnings("unchecked")
    protected <TT extends TestEntity> TT createEntityFromBytes(final byte[] p_bytes, final int p_offset, final int p_size) {
        try {
            var l_ret = m_createrTest.create(TestEntity.getTypeInt(p_bytes, p_offset));
            if (l_ret != null) return (TT) l_ret;
            EasyLog.showError("Неизвестный тип объекта");
            return null;
        } catch (Exception e) {
            EasyLog.showError(e);
            return null;
        }
    }

    @SuppressWarnings("unchecked")
    protected <TT extends IEntityGetterSetter> TT createEntityVersion(final TestEntity p_parent, final int p_version) {
        try {
            return (TT) m_createrTest.createVersion(p_parent, p_parent.getType(), p_version);
        } catch (Exception e) {
            EasyLog.showException(e);
            return null;
        }
    }

    @Override
    public T getFromId(final long p_id) {
        return super.getFromId(p_id);
    }

}
