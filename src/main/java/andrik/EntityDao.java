package andrik;

import andrik.stub.ResultSetImpl;

import java.sql.ResultSet;
import java.util.function.Consumer;

/**
 *
 * @author Eugene Bereskin mailto: <a href="mailto:john.boroda@gmail.com">john.boroda@gmail.com</a>
 * Created: 20.11.2021 17:23
 */
public class EntityDao {

    public void load(String selectQuery, Consumer<ResultSet> consumer) {
        consumer.accept(new ResultSetImpl());
    }

    public void update(String query) {
        System.out.println(query);
    }

}
