package andrik;

/**
 *
 * @author Eugene Bereskin mailto: <a href="mailto:john.boroda@gmail.com">john.boroda@gmail.com</a>
 * Created: 20.11.2021 16:59
 */
public record ModifyEvent(Class<?> clazz, FieldName field, Object oldValue, Object newValue) {
    @Override
    public String toString() {
        return "ModifyEvent{" +
                clazz.getName() +
                " '" + field + "' " +
                oldValue +
                " => " + newValue +
                '}';
    }
}
