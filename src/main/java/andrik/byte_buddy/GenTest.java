package andrik.byte_buddy;

import andrik.Model_;
import net.bytebuddy.ByteBuddy;
import net.bytebuddy.implementation.MethodDelegation;
import net.bytebuddy.implementation.bind.annotation.Argument;
import net.bytebuddy.implementation.bind.annotation.Origin;
import net.bytebuddy.implementation.bind.annotation.SuperCall;
import net.bytebuddy.matcher.ElementMatchers;

import java.lang.reflect.Method;
import java.util.concurrent.Callable;

/**
 *
 * @author Eugene Bereskin mailto: <a href="mailto:john.boroda@gmail.com">john.boroda@gmail.com</a>
 * Created: 21.11.2021 18:31
 */
public class GenTest {

    public static void main(String[] args) throws Exception {
        long start = System.currentTimeMillis();
        var modelClass = new ByteBuddy()
                .subclass(Model_.class)
                .method(ElementMatchers.nameStartsWith("set"))
                .intercept(MethodDelegation.to(new ModelInterceptor()))
                .make()
                .load(GenTest.class.getClassLoader())
                 .getLoaded();
                 
        System.out.println(modelClass.getName());
        var model = modelClass
                .getDeclaredConstructor()
                .newInstance();
        System.out.println(System.currentTimeMillis() - start);

        model.setId(1);
        model.setName("John");
        model.setValue("Boroda");

        System.out.println(model);

//        start = System.currentTimeMillis();
//        fun = s -> "Hello from " + s;
//        System.out.println(System.currentTimeMillis() - start);
//        System.out.println(fun.apply("boroda"));
//    }
    }

    public static class ModelInterceptor {
        public void intercept(@Argument(0) Object argument, @Origin Method method, @SuperCall Callable<?> callable) {
            System.out.printf("%s(%s)%n", method.getName(), argument);
            try {
                callable.call();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

}
