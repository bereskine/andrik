package andrik;

import java.util.*;

/**
 *
 * @author Eugene Bereskin mailto: <a href="mailto:john.boroda@gmail.com">john.boroda@gmail.com</a>
 * Created: 20.11.2021 17:14
 */
public interface ModifyObserver {
    
    void fireEvent(ModifyEvent modifyEvent);
    
}
