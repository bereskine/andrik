package andrik;

/**
 *
 * @author Eugene Bereskin mailto: <a href="mailto:john.boroda@gmail.com">john.boroda@gmail.com</a>
 * Created: 20.11.2021 16:24
 */
public interface Model {
    
    int id();
    
    void setId(int id);

    String name();
    
    void setName(String name);

    String value();
    
    void setValue(String value);
    
    void save();
}
