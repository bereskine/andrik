package andrik;

import java.util.*;

/**
 *
 * @author Eugene Bereskin mailto: <a href="mailto:john.boroda@gmail.com">john.boroda@gmail.com</a>
 * Created: 20.11.2021 17:20
 */
public class ModifyObserverImpl implements ModifyObserver {
    
    @Override
    public void fireEvent(ModifyEvent modifyEvent) {
        System.out.println(modifyEvent);
    }
    
}
