package andrik;

/**
 *
 * @author Eugene Bereskin mailto: <a href="mailto:john.boroda@gmail.com">john.boroda@gmail.com</a>
 * Created: 20.11.2021 18:13
 */
public class Test {

    public static void main(String[] args) throws Exception {
        ModelFactory modelFactory = new ModelFactory();

        Model model = modelFactory.load(Model.class);

        model.setId(1);
        model.setName("John");
        model.setValue("Boroda");

        model.save();

        model.setName("Eugene");
        model.save();
    }

}
