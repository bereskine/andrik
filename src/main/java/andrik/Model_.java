package andrik;

/**
 *
 * @author Eugene Bereskin mailto: <a href="mailto:john.boroda@gmail.com">john.boroda@gmail.com</a>
 * Created: 20.11.2021 16:21
 */
public class Model_  {
    
    @Field
    protected int id;
    
    @Field
    protected String name;
    
    @Field
    protected String value;

    public Model_() {
    }

    public Model_(int id, String name, String value) {
        this.id = id;
        this.name = name;
        this.value = value;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setValue(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return "Model_{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", value='" + value + '\'' +
                '}';
    }
}
