package andrik.generated;

import andrik.*;

import java.sql.SQLException;
import java.util.Arrays;
import java.util.Objects;

/**
 *
 * @author Eugene Bereskin mailto: <a href="mailto:john.boroda@gmail.com">john.boroda@gmail.com</a>
 * Created: 20.11.2021 16:21
 */
public class Model_Generated extends Model_ implements Model {

    private final String table = "public.model";

    private final String selectQuery = "select id, name, value from public.model";

    private enum Field implements FieldName {
        id, name, value
    }

    //private final Queue<ModifyEvent> modifyEvents = new ConcurrentLinkedDeque<>();
    private final ModifyObserver modifyObserver;
    private final EntityDao entityDao;

    private final boolean[] modifyStatus = new boolean[3];

    public Model_Generated(EntityDao entityDao, ModifyObserver modifyObserver) {
        super();
        this.modifyObserver = modifyObserver;
        this.entityDao = entityDao;

        entityDao.load(selectQuery, res -> {
            try {
                id = res.getInt(Field.id.name());
                name = res.getString(Field.name.name());
                value = res.getString(Field.value.name());
            } catch (SQLException e) {
                throw new IllegalStateException(e);
            }
        });
        System.out.println(this);
    }

    public void save() {
        var updateQuery = updateQuery();
        if (updateQuery.isEmpty()) return;
        entityDao.update(updateQuery);
        Arrays.fill(modifyStatus, false);
    }

    public String updateQuery() {
        var update = new StringBuilder();
        Field[] values = Field.values();
        int j = 0;
        for(int i = 0; i < values.length; i++) {
            if (!modifyStatus[i]) continue;
            j++;
            if (j > 1) {
                update.append(", ");
            }
            Field field = values[i];
            update.append(field.name()).append(" = ").append(valueOf(field));
        }
        if (update.length() == 0) {
            return "";
        }
        update.insert(0, " set ");
        update.insert(0, table);
        update.insert(0, "update ");
        return update.toString();
    }

    private Object valueOf(Field field) {
        return switch (field) {
            case id -> id;
            case name -> name;
            case value -> value;
        };
    }

    @Override
    public int id() {
        return id;
    }

    @Override
    public void setId(int id) {
        if (this.id == id) return;

        modifyObserver.fireEvent(new ModifyEvent(this.getClass(), Field.id, this.id, id));
        modifyStatus[Field.id.ordinal()] = true;

        this.id = id;
    }

    @Override
    public String name() {
        return name;
    }

    @Override
    public void setName(String name) {
        if (Objects.equals(this.name, name)) return;

        modifyObserver.fireEvent(new ModifyEvent(this.getClass(), Field.name, this.name, name));
        modifyStatus[Field.name.ordinal()] = true;

        this.name = name;
    }

    @Override
    public String value() {
        return value;
    }

    @Override
    public void setValue(String value) {
        if (Objects.equals(this.value, value)) return;

        modifyObserver.fireEvent(new ModifyEvent(this.getClass(), Field.value, this.value, value));
        modifyStatus[Field.value.ordinal()] = true;

        this.value = value;
    }

    @Override
    public String toString() {
        return "Model{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", value='" + value + '\'' +
                '}';
    }
}
