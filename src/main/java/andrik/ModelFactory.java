package andrik;

import andrik.generated.Model_Generated;

import java.util.*;
import java.util.function.BiFunction;
import java.util.function.Supplier;

/**
 *
 * @author Eugene Bereskin mailto: <a href="mailto:john.boroda@gmail.com">john.boroda@gmail.com</a>
 * Created: 20.11.2021 18:48
 */
public class ModelFactory {
    
    private Map<Class<?>, BiFunction<EntityDao, ModifyObserver, ?>> factory = new HashMap<>();

    private EntityDao entityDao;
    private ModifyObserverImpl modifyObserver;
    
    public ModelFactory() {
        entityDao = new EntityDao();
        modifyObserver = new ModifyObserverImpl();

        factory.put(Model.class, Model_Generated::new); 
    }

    public <T> T load(Class<T> clazz) {
        return (T) factory.get(clazz).apply(entityDao, modifyObserver); 
    };
    
}
